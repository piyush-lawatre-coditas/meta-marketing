const bizSdk = require('facebook-nodejs-business-sdk');
const AdAccount = bizSdk.AdAccount;
const Campaign = bizSdk.Campaign;

const access_token = '<ACCESS_TOKEN>';
const app_secret = '<APP_SECRET>';
const app_id = '<APP_ID>';
const ad_account_id = '<AD_ACCOUNT_ID>';

const api = bizSdk.FacebookAdsApi.init(access_token);
api.setAppSecret(app_secret);
api.setAppId(app_id);
api.setDebug(true);

const CampaignObjectives = {
	APP_INSTALLS: 'APP_INSTALLS',
	BRAND_AWARENESS: 'BRAND_AWARENESS',
	CONVERSIONS: 'CONVERSIONS',
	EVENT_RESPONSES: 'EVENT_RESPONSES',
	LEAD_GENERATION: 'LEAD_GENERATION',
	LINK_CLICKS: 'LINK_CLICKS',
	LOCAL_AWARENESS: 'LOCAL_AWARENESS',
	MESSAGES: 'MESSAGES',
	OFFER_CLAIMS: 'OFFER_CLAIMS',
	OUTCOME_APP_PROMOTION: 'OUTCOME_APP_PROMOTION',
	OUTCOME_AWARENESS: 'OUTCOME_AWARENESS',
	OUTCOME_ENGAGEMENT: 'OUTCOME_ENGAGEMENT',
	OUTCOME_LEADS: 'OUTCOME_LEADS',
	OUTCOME_SALES: 'OUTCOME_SALES',
	OUTCOME_TRAFFIC: 'OUTCOME_TRAFFIC',
	PAGE_LIKES: 'PAGE_LIKES',
	POST_ENGAGEMENT: 'POST_ENGAGEMENT',
	PRODUCT_CATALOG_SALES: 'PRODUCT_CATALOG_SALES',
	REACH: 'REACH',
	STORE_VISITS: 'STORE_VISITS',
	VIDEO_VIEWS: 'VIDEO_VIEWS'
};

const fields = [];
const params = {
	// Each object contains a fields map with a list of fields supported on that object.
	[Campaign.Fields.name]: 'My First Campaign', 
	[Campaign.Fields.status]: Campaign.Status.paused,
	[Campaign.Fields.objective]: Campaign.Objective.page_likes,
	special_ad_categories: []
};

const newCampaign = new AdAccount(ad_account_id).createCampaign(fields, params);
console.log('Campaign creation API call complete.', newCampaign);
