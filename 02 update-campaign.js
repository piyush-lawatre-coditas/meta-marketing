const bizSdk = require('facebook-nodejs-business-sdk');
const Campaign = bizSdk.Campaign;
const campaignId = '<CAMPAIGN_ID>';
const access_token = '<ACCESS_TOKEN>';
const app_secret = '<APP_SECRET>';
const app_id = '<APP_ID>';

const api = bizSdk.FacebookAdsApi.init(access_token);
api.setAppSecret(app_secret);
api.setAppId(app_id);
api.setDebug(true);

const fieldsToUpdate = {
	[Campaign.Fields.name]: 'Updated Campaign Name',
	[Campaign.Fields.status]: Campaign.Status.active
};

const updatedCampaign = new Campaign(campaignId).update(fieldsToUpdate);
console.log('Campaign update API call complete.', updatedCampaign);
