//INFO: High level insights, including impressions, clicks and the amount spent
//INFO: Optionally specifying the start/end time:

const { FacebookAdsApi, AdsInsightsFields, Campaign } = require('facebook-nodejs-business-sdk');

const access_token = 'YOUR_ACCESS_TOKEN';
const app_secret = 'YOUR_APP_SECRET';
const app_id = 'YOUR_APP_ID';
const campaign_id = 'YOUR_CAMPAIGN_ID';

const api = FacebookAdsApi.init(access_token);

api.setAppSecret(app_secret);
api.setAppId(app_id);
api.setDebug(true);

const campaign = new Campaign(campaign_id);

const now = Math.floor(Date.now() / 1000);
const params = {
    'start_date':1702291717,
    'end_time': now,

};

const fields = [
  AdsInsightsFields.IMPRESSIONS,
  AdsInsightsFields.INLINE_LINK_CLICKS,
  AdsInsightsFields.SPEND
];

campaign.getInsights(fields, params).then(insights => {
  console.log(insights);
}).catch(err => {
  console.error(err);
});
