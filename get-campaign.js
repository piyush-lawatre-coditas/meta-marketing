'use strict';

const bizSdk = require('facebook-nodejs-business-sdk');
const AdAccount = bizSdk.AdAccount;

const adAccountId = '<AD_ACCOUNT_ID>';

function getCampaigns() {
  const fields = ['name', 'objective'];
  const params = {
    'effective_status': ['ACTIVE', 'PAUSED'],
  };
  return (new AdAccount(adAccountId)).getCampaigns(fields, params);
}

function execute() {
  const campaigns = getCampaigns();
  campaigns
    .then((result) => {
      console.log('Campaigns API call complete.', result);
    })
    .catch((error) => {
      console.error('Error fetching campaigns:', error);
    });
}

execute();
//Link : https://developers.facebook.com/docs/marketing-api/campaign-structure#
